# WheatNet On-Air Source Monitor

This WheatNet IP script periodically polls WheatNet to determine the current sources
which are being used for the main (FM) stream and HD2 stream. Specifically, it will
query the current connection to STR_MAIN and STR_HD2, which are the destinations on
the Engineering Room blade (E_AD) that supply the audio to the rack-mount Scarlett
for the online streams (and archives).

Every 10 seconds, it will write the source signals of those two destinations to a
file named `wheat_sources.txt`. This file can be read by other scripts to make
sure the song metadata from AudioVault is properly handled.

View/edit this script with the WheatNet IP Screens Builder program.
