# Debugging Notes

Windows CMD script I used for debugging:

```shell
@echo off

set SCRIPT_NAME=hello_world.wnipscr

:: reg add "HKCU\SOFTWARE\Wheatstone\WheatNetIp_ScreenEngine\%SCRIPT_NAME%" /F /V alwaysOnTop /T REG_SZ /D "false"
:: reg add "HKCU\SOFTWARE\Wheatstone\WheatNetIp_ScreenEngine\%SCRIPT_NAME%" /F /V logFile /T REG_SZ /D "%cd%\OUT.TXT"
:: reg add "HKCU\SOFTWARE\Wheatstone\WheatNetIp_ScreenEngine\%SCRIPT_NAME%" /F /V logFilters /T REG_SZ /D "SSVM"
:: reg add "HKCU\SOFTWARE\Wheatstone\WheatNetIp_ScreenEngine\%SCRIPT_NAME%" /F /V logLines /T REG_DWORD /D 500
:: reg add "HKCU\SOFTWARE\Wheatstone\WheatNetIp_ScreenEngine\%SCRIPT_NAME%" /F /V logtoFile /T REG_SZ /D "true"
:: reg add "HKCU\SOFTWARE\Wheatstone\WheatNetIp_ScreenEngine\%SCRIPT_NAME%" /F /V posLock /T REG_SZ /D "false"
:: reg add "HKCU\SOFTWARE\Wheatstone\WheatNetIp_ScreenEngine\%SCRIPT_NAME%" /F /V posX /T REG_DWORD /D 0
:: reg add "HKCU\SOFTWARE\Wheatstone\WheatNetIp_ScreenEngine\%SCRIPT_NAME%" /F /V posY /T REG_DWORD /D 0

"C:\Program Files (x86)\Wheatstone\WheatNetIpScreens\WheatNetIpScreenEngine.exe" -s %SCRIPT_NAME%
```

Set `SCRIPT_NAME` to the name of your script.

If you need to see the output debug logs from the script, un-comment the `reg add` lines.
The output will be available at `OUT.TXT`.